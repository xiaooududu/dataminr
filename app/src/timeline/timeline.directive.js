(function() {
    'use strict';

    function TimelineDirective() {
        return {
            templateUrl: 'src/timeline/timeline.html',
            strict: 'E',
            controller: 'TimelineController',
            controllerAs: 'ctrl',
            bindToController: true,
            scope: {
                timelineEntries: '='
            }
        };
    }

    angular.module('dataminr').directive('timeline', TimelineDirective);

})();