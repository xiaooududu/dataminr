(function() {
    'use strict';

    function MainPageDirective() {
        return {
            templateUrl: 'src/main-page/main-page.html',
            strict: 'E',
            controller: 'MainPageController',
            controllerAs: 'ctrl',
            bindToController: true
        };
    }

    angular.module('dataminr').directive('main', MainPageDirective);

})();