(function() {
    'use strict';

    function MainPageController(TwitterAuthenticationService,
                                TwitterTimelineService, TimelineType,
                                MostCommonlyUsedTermsService) {
        var self = this;

        self.timelineType = _.clone(TimelineType);

        self.isCurrentlyLoggedIn = function() {
            return TwitterAuthenticationService.getLoggedIn();
        };

        var loading = false;
        self.requestForTweets = function(type) {
            loading = true;
            self.timeline = [];
            self.terms = [];
            var credentials = TwitterAuthenticationService.getAccessToken();
            TwitterTimelineService.getTimelineAsync({
                credentials: credentials,
                type: type
            })
                .then(function(timeline){
                    self.timeline = timeline;
                    self.terms = MostCommonlyUsedTermsService.getTopUsedTerms({
                        number: 10,
                        textArray: _.map(self.timeline, 'text')
                    });
                })
                .finally(function() {
                    loading = false;
                });
        };

        self.loading = function() {
            return loading;
        }
    }

    angular.module('dataminr')
        .controller('MainPageController',
        ['TwitterAuthenticationService',
            'TwitterTimelineService',
            'TimelineType',
            'MostCommonlyUsedTermsService',
            MainPageController]);
})();