(function(){
    'use strict';

    angular.module('dataminr').directive('application', function(){
        return{
            templateUrl: 'src/application/application.html',
            strict: 'E',
            controller: function() {

            }
        }
    })
})();