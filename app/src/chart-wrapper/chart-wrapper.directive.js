(function(){
    'use strict';

    function ChartWrapperDirective() {
        return {
            require: 'ngModel',
            templateUrl: 'src/chart-wrapper/chart-wrapper.html',
            strict: 'E',

            controller: function() {

            },
            controllerAs: 'ctrl',
            scope: {
                chartData: '=',
                selected: '=ngModel'
            },
            link: function(scope, element, attrs, ngModelController){
                scope.select = function(item){
                    if(_.isString(item)){
                        ngModelController.$setViewValue(item);
                    }

                    if( _.isUndefined(ngModelController.$viewValue)){
                        return ngModelController.$viewValue;
                    }

                    return item;
                }
            }
        }
    }

    angular.module('dataminr').directive('chartWrapper', ChartWrapperDirective);
})();