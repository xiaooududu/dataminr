(function(){
    'use strict';

    var count = 100;

    function getHomeTimelineParams(){
        return {};
    }
    function getUserTimelineParams(credentials){
        return {
            screen_name: credentials.screen_name
        }
    }
    function getMentionsTimelineParams(){
        return {};
    }
    function getRetweetsTimelineParams() {
        return {};
    }


    function TwitterTimelineFactory( $http, TimelineType ) {
        function generateRequestParameters(options) {
            var type = options.type;

            var accessToken = options.credentials;
            var param = {};

            if(type === TimelineType.Home) {
                param = getHomeTimelineParams();
            } else if (type === TimelineType.User) {
                param = getUserTimelineParams(accessToken.results);
            } else if (type === TimelineType.Mentions) {
                param = getMentionsTimelineParams();
            } else if (type === TimelineType.Retweets) {
                param = getRetweetsTimelineParams();
            } else {
                throw new Error('unknown timeline type');
            }
            param.count = count;
            param.type = type;

            param.accessToken = accessToken.accessToken;
            param.accessTokenSecret= accessToken.accessTokenSecret;

            return param;
        }

        return {
            getTimelineAsync: function(options) {
                var params = generateRequestParameters(options);

                return $http.get('https://homework.dataminr.com/getTwitterTimeline', {
                    params: params
                }).then(function(result){
                    var timeline = result.data;
                    if(!_.isUndefined(timeline.error)){
                        throw new Error(JSON.stringify(timeline.error));
                    }
                    return timeline;
                })
                    .catch(function(){
                        return [];
                    });
            }
        };
    }
    angular.module('dataminr')
        .factory('TwitterTimelineService',
        ['$http', 'TimelineType', TwitterTimelineFactory]);
})();