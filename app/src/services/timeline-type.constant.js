(function(){
    'use strict';

    angular.module('dataminr')
        .constant('TimelineType', {
            Home: 'home',
            User: 'user',
            Mentions: 'mentions',
            Retweets: 'retweets'
        });
})();