(function(){
    'use strict';

    function isWordExcluded(word) {
        if(word.length <= 2){
            return true;
        }
        if(!_.isUndefined(excludedWords[word])){
            return true;
        }
        return false;
    }

    var excludedWords = {
        the: 0,
        for: 0,
        about: 0,
        are: 0,
        com: 0,
        from: 0,
        how: 0,
        that: 0,
        this: 0,
        was: 0,
        what: 0,
        when: 0,
        where: 0,
        who: 0,
        will: 0,
        with: 0,
        www: 0,
        and: 0,
        you: 0,
        your: 0
    };

    function processText(text, words) {
        var wordRegExp = /\w+(?:'\w{1,2})?/g;
        var matches;
        while ((matches = wordRegExp.exec(text)) != null)
        {
            var word = matches[0].toLowerCase();
            if (!isWordExcluded((word))) {
                if (_.isUndefined(words[word])) {
                    words[word] = 0;
                }
                words[word]++;
            }
        }
    }

    function MostCommonlyUsedTermsFactory() {
        return {
            getTopUsedTerms: function(options){
                var n = options.number;
                var textArray = options.textArray;

                var words = {};
                _.each(textArray, function(text){
                    processText(text, words);
                });

                var wordList = [];
                _.each(words, function(count, word){
                    wordList.push([word, count]);
                });
                wordList.sort(function(a, b) { return b[1] - a[1]; });

                return _.first(wordList, 10);
            }
        }
    }

    angular.module('dataminr').factory('MostCommonlyUsedTermsService', MostCommonlyUsedTermsFactory);
})();