(function() {
    'use strict';

    var nyTimesApiKey = 'c4283557537d63e4fe0b0b70a65277d0:8:74063897';

    function RelatedNewsFactory($http) {
        return {
            getRelatedNewsAsync: function(options) {
                var keyword = options.keyword;
                var fromDate = options.date;
                return $http.get('http://api.nytimes.com/svc/search/v2/articlesearch.json', {
                    params: {
                        fq: 'headline("'+keyword+'")',
                        'api-key': nyTimesApiKey,
                        begin_date: fromDate
                    }
                })
                    .then(function(result) {
                        if(result.data.status !== 'OK'){
                            throw new Error('Ny time api throw an error')
                        }

                        return result.data.response.docs;
                    })
                    .catch(function(error){
                        return [];
                    });
            }
        }
    }

    angular.module('dataminr').factory('RelatedNewsService', ['$http', RelatedNewsFactory]);
})();