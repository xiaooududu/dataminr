(function(){
    'use strict';

    var requestTokenKey = '$$TWITTER_REQUEST_TOKEN$$';
    var accessTokenKey = '$$TWITTER_ACCESS_TOKEN$$';
    var userInfoKey = '$$TWITTER_LOGGED_USER_INFO$$';
    var callbackUrlKey = '$$TWITTER_DISPLAY_APP_URL$$';

    function getItem(key){
        return JSON.parse(window.localStorage.getItem(key));
    }
    function setItem(key, value){
        window.localStorage.setItem(key, JSON.stringify(value));
    }

    function TwitterAuthenticationFactory( $http, $location ) {
        return {
            sendRequestTokenRequest: function() {
                var callbackUrl = $location.absUrl();
                window.localStorage.setItem(callbackUrlKey, callbackUrl);
                return $http.get('https://homework.dataminr.com/getTwitterRequestToken',
                    {
                        params: {
                            callback:callbackUrl
                        }
                    })
                    .then(function(result){
                        var requestToken = result.data;

                        if(!_.isUndefined(requestToken.error)) {
                            throw new Error(JSON.stringify(requestToken.error));
                        }

                        setItem(requestTokenKey, requestToken);
                        window.location.href = 'https://api.twitter.com/oauth/authorize?oauth_token='
                            + requestToken.requestToken;
                    })
                    .catch(function(error) {
                        $log.info(error)
                    });
            },
            getLoggedIn: function() {
                var userInfo = getItem(userInfoKey);
                return userInfo !== null;
            },
            sendUserCredentialRequests: function() {
                var oauthToken = $location.search();
                if(!_.isEqual(oauthToken, {})) {
                    var requestToken = getItem(requestTokenKey);
                    $http.get('https://homework.dataminr.com/getTwitterAccessToken',
                        {
                            params: {
                                requestToken:requestToken.requestToken,
                                requestTokenSecret:requestToken.requestTokenSecret,
                                oauth_verifier:oauthToken.oauth_verifier
                            }
                        })
                        .then(function(result){
                            var accessToken = result.data;

                            if(!_.isUndefined(accessToken.error)){
                                throw new Error(JSON.stringify(accessToken.error));
                            }

                            setItem(accessTokenKey, accessToken);
                            return $http.get('https://homework.dataminr.com/getTwitterCredentials', {
                                params: {
                                    accessToken:accessToken.accessToken,
                                    accessTokenSecret:accessToken.accessTokenSecret
                                }
                            });
                        })
                        .then(function(result){
                            var userInfo = result.data;

                            if(!_.isUndefined(userInfo.error)){
                                throw new Error(JSON.stringify(userInfo.error));
                            }

                            setItem(userInfoKey, userInfo);
                            window.location.href = window.localStorage.getItem(callbackUrlKey);
                        });
                }
            },
            removeUserCredentials: function(){
                window.localStorage.removeItem(requestTokenKey);
                window.localStorage.removeItem(accessTokenKey);
                window.localStorage.removeItem(userInfoKey);
                window.localStorage.removeItem(callbackUrlKey);
            },
            getAccessToken: function() {
                return getItem(accessTokenKey);
            },
            getUserCredentials: function () {
                return getItem(userInfoKey);
            }
        };
    }
    angular.module('dataminr')
        .factory('TwitterAuthenticationService',
        ['$http', '$location', TwitterAuthenticationFactory]);
})();