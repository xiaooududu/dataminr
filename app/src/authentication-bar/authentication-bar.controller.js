(function(){
    'use strict';

    function AuthenticationBarController(TwitterAuthenticationService) {
        var self = this;

        self.logIn = function() {
            TwitterAuthenticationService.sendRequestTokenRequest();
        };
        self.logOff = function() {
            TwitterAuthenticationService.removeUserCredentials();
        };

        self.isCurrentlyLoggedIn = function() {
            return TwitterAuthenticationService.getLoggedIn();
        };

        self.getUserName = function() {
            var userCredentials = TwitterAuthenticationService.getUserCredentials();
            return userCredentials.name;
        };

        TwitterAuthenticationService.sendUserCredentialRequests();
    }

    angular.module('dataminr').controller('AuthenticationBarController',
        ['TwitterAuthenticationService', AuthenticationBarController]);
})();