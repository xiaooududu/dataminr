(function(){
    'use strict';

    function AuthenticationBarFactory() {
        return {
            templateUrl: 'src/authentication-bar/authentication-bar.html',
            strict: 'E',
            controller: 'AuthenticationBarController',
            controllerAs: 'ctrl',
            link: function(scope, attr, ele, controller){
                scope.logIn = function() {
                    controller.logIn();
                };
                scope.logOff = function() {
                    controller.logOff();
                };

                scope.getUserName = function() {
                    return controller.getUserName();
                };
            }
        }
    }

    angular.module('dataminr').directive('authenticationBar', AuthenticationBarFactory);
})();