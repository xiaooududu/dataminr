(function() {
    'use strict';

    function NewsDirective() {
        return {
            templateUrl: 'src/news/news.html',
            strict: 'E',
            controller: 'NewsController',
            controllerAs: 'ctrl',
            bindToController: true,
            scope: {
                newsInfo: '='
            }
        }

    }

    angular.module('dataminr').directive('news', NewsDirective);
})();