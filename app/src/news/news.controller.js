(function() {
    'use strict';

    function NewsController() {
        var self = this;

        self.abstract = function(){
            return self.newsInfo.abstract;
        };

        self.headline = function(){
            return self.newsInfo.headline.main;
        };

        self.url = function() {
            return self.newsInfo['web_url'];
        };

        self.source = function() {
            return self.newsInfo.source;
        };

        self.time = function() {
            var dateTime = new Date(self.newsInfo['pub_date']);
            return dateTime.toDateString() + ' ' + dateTime.toTimeString();
        }

    }

    angular.module('dataminr')
        .controller('NewsController', NewsController);
})();