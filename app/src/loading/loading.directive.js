(function() {
    'use strict';

    function LoadingDirective() {
        return {
            template: '<div>loading...</div>'
        }
    }

    angular.module('dataminr').directive('loading', LoadingDirective);
})();