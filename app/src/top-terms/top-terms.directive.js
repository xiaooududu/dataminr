(function() {
    'use strict';

    function TopTermsDirective() {
        return {
            templateUrl: 'src/top-terms/top-terms.html',
            strict: 'E',
            controller: 'TopTermsController',
            controllerAs: 'ctrl',
            bindToController: true,
            scope: {
                topTermEntries: '='
            }
        }

    }

    angular.module('dataminr').directive('topTerms', TopTermsDirective);
})();