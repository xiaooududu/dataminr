(function() {
    'use strict';

    function TopTermsController() {
        var self = this;

        self.changeSelectedTerm = function(term){
            self.selectedTerm = term;
        }
    }

    angular.module('dataminr')
        .controller('TopTermsController', TopTermsController);
})();