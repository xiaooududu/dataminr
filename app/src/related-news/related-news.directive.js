(function(){
    'use strict';

    function RelatedNewsDirective(){
        return {
            templateUrl: 'src/related-news/related-news.html',
            strict: 'E',
            controller: 'RelatedNewsController',
            controllerAs: 'ctrl',
            bindToController: true,
            scope: {
                term: '='
            }
        };
    }

    angular.module('dataminr').directive('relatedNews', RelatedNewsDirective);
})();