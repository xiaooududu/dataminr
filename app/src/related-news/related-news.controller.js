(function(){
    'use strict';

    Date.prototype.yyyymmdd = function() {
        var yyyy = this.getFullYear().toString();
        var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
        var dd  = this.getDate().toString();
        return yyyy + (mm[1]?mm:"0"+mm[0]) + (dd[1]?dd:"0"+dd[0]); // padding
    };

    function RelatedNewsController($scope, RelatedNewsService) {
        var self = this;

        var loading = false;
        self.getTopNewsRelatedToTerm = function(term){
            var date = new Date();
            date.setDate(date.getDate() - 7);

            RelatedNewsService.getRelatedNewsAsync({
                date: date.yyyymmdd(),
                keyword: term
            })
                .then(function(result) {
                    self.news = result;
                    loading = false;
                });
            loading = true;
            self.news = [];
        };

        self.loading = function(){
            return loading;
        };

        $scope.$watch(function() {
            return self.term;
        }, function(newValue, oldValue){
            if(newValue !== oldValue) {
                self.getTopNewsRelatedToTerm(newValue);
            }
        }, true);

    }

    angular.module('dataminr').controller('RelatedNewsController', ['$scope', 'RelatedNewsService', RelatedNewsController]);
})();