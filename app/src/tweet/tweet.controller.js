(function() {
    'use strict';

    function TweetController() {
        var self = this;

        self.userName = function(){
            return self.tweetInfo.user.screen_name;
        };

        self.userUrl = function(){
            return self.tweetInfo.user.url;
        };

        self.text = function() {
            return self.tweetInfo.text;
        };

        self.time = function() {
            var dateTime = new Date(self.tweetInfo.created_at);
            return dateTime.toDateString() + ' ' + dateTime.toTimeString();
        };

    }

    angular.module('dataminr')
        .controller('TweetController', TweetController);
})();