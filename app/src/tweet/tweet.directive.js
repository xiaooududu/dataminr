(function() {
    'use strict';

    function TweetDirective() {
        return {
            templateUrl: 'src/tweet/tweet.html',
            strict: 'E',
            controller: 'TweetController',
            controllerAs: 'ctrl',
            bindToController: true,
            scope: {
                tweetInfo: '='
            }
        }

    }

    angular.module('dataminr').directive('tweet', TweetDirective);
})();