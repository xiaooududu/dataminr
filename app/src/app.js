'use strict';

/**
 * @ngdoc overview
 * @name dataminr
 * @description
 * # dataminr
 *
 * Main module of the application.
 */
angular
    .module('dataminr', ['ngSanitize'])
    .config(['$locationProvider', function($locationProvider) {
      $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
      });
    }]);
